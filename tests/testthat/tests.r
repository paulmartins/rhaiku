context("Classes output")

test_that("haiku and aurelius are characters",{
	expect_is(haiku(),'character')
	expect_is(aurelius(),'character')
})