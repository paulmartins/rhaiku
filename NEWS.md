# Rhaiku 1.0.0

* Added a `NEWS.md` file to track changes to the package.
* Add functions `haiku()` and `aurelius()`

